
from flask import Flask, session, render_template
import os
from functools import wraps

app = Flask(__name__)
app.secret_key = os.urandom (12)


def check_logged_in(func):
   @wraps(func)
   def wrapper(*args, **kwargs):
      if 'username' in session:
         return func(*args, **kwargs)
      return render_template('error.html',
                             the_title='Nie jesteś zalogowany', )
   return wrapper