
from flask import Flask, render_template, request, escape, copy_current_request_context, session
import os
import pandas
import mysql.connector
from sqlalchemy import create_engine
from DBasemyapp import UseDatabase, ConnectionError, CredentialsError, SQLError
from checker import check_logged_in


from threading import Thread

app = Flask(__name__)

app.secret_key = os.urandom (12)

app.config['dbconfig'] = {'host': '127.0.0.1',
                'user': 'pracownik',
                'password': 'pwd',
                'database': 'Myapplog', }


@app.route('/zgloszenie')
@check_logged_in
def add_event() -> 'html':
    return render_template ('zgloszenie.html',
                            the_title='Zgłoszenie zdarzenia')


@app.route('/add', methods=['POST'])
def confirmation() -> 'html':
    @copy_current_request_context
    def log_request(req: 'flask_request') -> None:
        with UseDatabase (app.config['dbconfig']) as cursor:
            _SQL = """insert into mylog
                (komorka, rodzaj, opis, ip)
                values
                (%s, %s, %s, %s)"""
            cursor.execute(_SQL, (req.form['komorka'],
                                   req.form['rodzaj'],
                                   req.form['opis_sytuacji'],
                                   req.remote_addr, ))

    komorka = request.form['komorka']
    rodzaj = request.form['rodzaj']
    opis = request.form['opis_sytuacji']
    title = 'Zgłoszenie przyjęte'
    log_request(request)
    try:
        t = Thread(target=log_request, args=(request))
        t.start()
    except Exception as err:
        return render_template ('error.html',
                                the_title = "Dodanie zdarzenia nie powiodło się, błąd : ', str(err))", )
    return render_template ('potwierdzenie.html',
                            the_title=title,
                            the_komorka=komorka,
                            the_rodzaj=rodzaj,
                            the_zdarzenie=opis, )


@app.route('/viewlog')
@check_logged_in
def view_the_log() -> 'html':
    #wyświetla logi, mogą wystapić błedy, zwróci komunikat błędu#
    try:
        with UseDatabase(app.config['dbconfig']) as cursor:
            _SQL = """select id, ts, komorka, rodzaj, opis from mylog"""
            cursor.execute(_SQL)
            contents = cursor.fetchall()
            titles = ('ID',
                         'Czas zgoszenia',
                        'Komórka zgłaszająca',
                         'Rodzaj zdarzenia',
                         'Opis', )
        return render_template('viewlog.html',
                               the_title = 'Zgłoszenia zdarzeń niepożadanych',
                               the_row_titles=titles,
                               the_data=contents, )
    except ConnectionError as err:
        print('Czy połączenie z bazą działa? Błąd: ', str(err))
    except CredentialsError as err:
        print('Problem z identyfikatorem lub hasłem dostępu')
    except SQLError as err:
        print('Czy twoje zapytanie jest poprawne? Błąd:', str())
    except Exception as err:
        print('Coś poszło nie tak. Błąd: ', str(err))
    return 'Error'


@app.route ('/')
def home():
    if not session.get('username'):
        return render_template ('login.html')
    else:
        return render_template('zgloszenie.html')

@app.route ('/login', methods=['POST', 'GET'])
def check_log() -> 'html':
    try:
        def get_data_from_form(req: 'flask_request') -> 'str':
            with UseDatabase (app.config['dbconfig']) as cursor:
                _SQL = """SELECT password FROM users WHERE username = %s"""
                cursor.execute (_SQL, (req.form['username'],))
                content = str (cursor.fetchone ()[0])
                return content
        post_username = request.form['username']
        post_password = request.form['password']
        pwd = get_data_from_form (request)
        if pwd == post_password:
            session['username'] = True
            return render_template ('zgloszenie.html')
        else:
            return render_template ('error.html',
                                    the_title = 'Problem z identyfikatorem lub hasłem dostępu', )
    except:
        return render_template ('error.html',
                                the_title='Problem z identyfikatorem lub hasłem dostępu', )

@app.route ('/logout')
def logout():
    session.clear()
    return render_template('error.html',
                                  the_title = 'Jesteś wylogowany', )

@app.route('/print')
@check_logged_in
def print_report():
    engine = create_engine('mysql+mysqlconnector://pracownik:pwd@localhost/Myapplog')
    contents = pandas.read_sql('select id, ts, komorka, rodzaj, opis from mylog',  engine,
                            index_col=None,
                            coerce_float=True,
                            parse_dates=None,
                            chunksize=None)
    contents.to_excel('C:\webapp_my\printy.xlsx', sheet_name='first')
    return render_template('print.html',
                          the_contents = 'Plik został utworzony', )




if __name__ == '__main__':
    app.run (debug=True)
